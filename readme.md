## Music Playlist Service


### Description
This is a music playlist service where user can store and retrieve a number of music tracks.
# How to run

* Open this project in any editor e.g. visual studio code
* Run this command into your editor bash terminal 

```bash
npm install
```
* After that run this command, which will start your server

```bash
npm start
```

* Then visit `http://localhost:3000`





