import { application } from './App';
import connectDB from './config/dbHelper';

const port = 3000;

console.log(`Starting server..`);
application.listen(port, postStartupCallback);

// Connect to MongoDB
connectDB().then(() => {
  console.log("MongoDB Connected...");
}).catch((error) => {
  console.log('unable to connet to mongoDb');
  console.log(error);
  process.exit(1);
});

/**
 * Callback function for everything that runs after the server has successfully started up
 */
function postStartupCallback() {
  console.log(`Server is available at http://localhost:3000`);
}

process.on('unhandledRejection', (reason, p) => {
  console.error(reason, 'Unhandled Rejection at Promise', p);
});
process.on('uncaughtException', err => {
  console.error(err, 'Uncaught Exception thrown');
  process.exit(1);
});