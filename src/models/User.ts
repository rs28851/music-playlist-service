import { Document, Model, model, Schema } from "mongoose";

/**
 * Interface to model the user Schema
 */
export interface IUser extends Document {
    userId: string;
    userName: string;
}

const userSchema: Schema = new Schema({
    userId: {
        type: String,
        required: true,
        unique: true
    },
    userName: {
        type: String,
        required: true
    }
});

export const User: Model<IUser> = model("user", userSchema);


/**
 * Represent format of adding user data
 */
export interface IAddUserData {
    userId: string;
    userName: string; 
}