import { Document, Model, model, Schema } from "mongoose";

/**
 * Interface to model the token Schema
 */
export interface IToken extends Document {
    tokenVal: string;
}

const tokenSchema: Schema = new Schema({
    tokenVal: {
        type: String,
        required: true
    },
    expireAt: {
        type: Date,
        default: Date.now,
        index: { expires: '12h' },
      }
});

export const Token: Model<IToken> = model("token", tokenSchema);