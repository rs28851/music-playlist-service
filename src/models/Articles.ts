
/**
 * Represent response of article 
 */
export interface IArticleResponse {
    articleId: number;
    articleName: string;
    artistName: string;
    artistId: number;
    duration: number;
}

/**
 * Represent response of a playlist with article information 
 */
export interface IPlaylistArticlesResponse {
    playListName: string;
    userId: string;
    userName: string;
    articles?: IArticleResponse[] // article ids
}

