
import { Document, Model, model, Schema } from "mongoose";

/**
 * Interface to model the Playlist Schema
 */
export interface IPlaylist extends Document {
    name: string; // playlist name
    id: string; // playlist id
    userId: string; // user id
    articleIds?: number[] // article ids
}

const playlistSchema: Schema = new Schema({
    id: {
        type: String,
        required: true,
        unique: true
    },
    name: {
        type: String,
        required: true
    },
    userId: {
        type: String,
        required: true
    },
    articleIds: {
        type: Array,
        default: []
    }
});

playlistSchema.index({name: 1, userId: 1}, {unique: true});
export const Playlist: Model<IPlaylist> = model("playlist", playlistSchema);

/**
 * Represent response format of all playlist data
 */
export interface IAllPlayListResponse {
    playlistId: string;
    playlisName: string;
    userId: string;
    userName: string;
    articleIds: number[]; 
}

/**
 * Represent format of adding playlist data
 */
export interface IAddPlayListData {
    userId: string;
    playListName: string; 
}

