import { Request, Response } from 'express';
import { BadRequestError } from '../error/BadRequestError';
import { GetArticlesDataError } from '../error/GetArticlesDataError';
import { NotFoundError } from '../error/NotFoundError';
import { ResourceConflictError } from '../error/ResourceConflictError';
import { playListService } from '../services/PlayListService';
import { IAddPlayListData } from '../models/Playlist';

export class PlaylistController {
    /**
     * this method will create playlist 
     */
    public async createPlaylist(req: Request, res: Response) {
        try {

            if ((!req.body.name || !req.params.userId)) {
                throw new BadRequestError();
            }
            const addPlayListData: IAddPlayListData = {
                playListName: req.body.name,
                userId: req.params.userId
            };
            await playListService.createPlayList(addPlayListData);
            res.status(201).json('Successfully created a playlist');
        } catch (e) {
            if (e instanceof BadRequestError) {
                res.status(e.statusCode).json({
                    statusCode: e.statusCode,
                    message: e.message
                });    
            } else if (e instanceof ResourceConflictError) {
                res.status(e.statusCode).json({
                    statusCode: e.statusCode,
                    message: e.message
                });    
            } else {
                res.status(500).json({
                    statusCode: 500,
                    message: 'Internal server error'
                });
            }
        }
    }

    /**
     * this method fetch all playlist
     */
    public async getAllPlaylist(req: Request, res: Response) {


        try {
            if (!req.params.userId) {
                throw new BadRequestError();
            }
            const allPlaylist = await playListService.getAllPlayListData(req.params.userId);
            res.status(200).json(allPlaylist);
        } catch (e) {
            if (e instanceof BadRequestError) {
                res.status(e.statusCode).json({
                    statusCode: e.statusCode,
                    message: e.message
                });    
            } else if (e instanceof NotFoundError) {
                res.status(e.statusCode).json({
                    statusCode: e.statusCode,
                    message: e.message
                });
            }
             else {
                res.status(500).json({
                    statusCode: 500,
                    message: 'Internal server error'
                });
            }
        }
    }
    /**
     * this method update a playlist
     */
    public async updatePlaylist(req: Request, res: Response) {

        try {
            if ((!req.params.playlistId || !req.params.userId || !req.body.name)) {
                throw new BadRequestError();
            }
            await playListService.updatePlaylistData(req.params.playlistId, req.params.userId, req.body.name);
            res.status(204).send();
        } catch (e) {
            if (e instanceof BadRequestError) {
                res.status(e.statusCode).json({
                    statusCode: e.statusCode,
                    message: e.message
                });    
            } else if (e instanceof NotFoundError) {
                res.status(e.statusCode).json({
                    statusCode: e.statusCode,
                    message: e.message
                });
            } else if (e instanceof ResourceConflictError) {
                res.status(e.statusCode).json({
                    statusCode: e.statusCode,
                    message: e.message
                });    
            } else {
                res.status(500).json({
                    statusCode: 500,
                    message: 'Internal server error'
                });
            }
        }
    }
    /**
     * this method delete a playlist
     */
    public async deletePlaylist(req: Request, res: Response) {

        try {
            if ((!req.params.playlistId || !req.params.userId)) {
                throw new BadRequestError();
            }
            await playListService.deletePlaylist(req.params.playlistId, req.params.userId);
            res.status(204).send();
        } catch (e) {
            if (e instanceof BadRequestError) {
                res.status(e.statusCode).json({
                    statusCode: e.statusCode,
                    message: e.message
                });    
            } else if (e instanceof NotFoundError) {
                res.status(e.statusCode).json({
                    statusCode: e.statusCode,
                    message: e.message
                });
            }
             else {
                res.status(500).json({
                    statusCode: 500,
                    message: 'Internal server error'
                });
            }
        }
    }
    /**
     * this method add an article into a playlist
     */
    public async addArticleIntoPlaylist(req: Request, res: Response) {

        try {
            if ((!req.params.playlistId || !req.params.userId || !req.params.articleId)) {
                throw new BadRequestError();
            }
            const articleId = parseInt(req.params.articleId);
            await playListService.addArticleIntoPlaylist(req.params.userId, req.params.playlistId, articleId);
            res.status(204).send();
        } catch (e) {
            if (e instanceof BadRequestError) {
                res.status(e.statusCode).json({
                    statusCode: e.statusCode,
                    message: e.message
                });    
            } else if (e instanceof NotFoundError) {
                res.status(e.statusCode).json({
                    statusCode: e.statusCode,
                    message: e.message
                });
            } else if (e instanceof ResourceConflictError) {
                res.status(e.statusCode).json({
                    statusCode: e.statusCode,
                    message: e.message
                });
            }
             else {
                res.status(500).json({
                    statusCode: 500,
                    message: 'Internal server error'
                });
            }
        }
    }
    /**
     * this method remove an article from a playlist
     */
    public async removeArticleFromPlaylist(req: Request, res: Response) {

        try {
            if ((!req.params.playlistId || !req.params.userId || !req.params.articleId)) {
                throw new BadRequestError();
            }
            const articleId = parseInt(req.params.articleId);
            await playListService.removeArticleFromPlaylist(req.params.userId, req.params.playlistId, articleId);
            res.status(204).send();
        } catch (e) {
            if (e instanceof BadRequestError) {
                res.status(e.statusCode).json({
                    statusCode: e.statusCode,
                    message: e.message
                });    
            } else if (e instanceof NotFoundError) {
                res.status(e.statusCode).json({
                    statusCode: e.statusCode,
                    message: e.message
                });
            }
             else {
                res.status(500).json({
                    statusCode: 500,
                    message: 'Internal server error'
                });
            }
        }
    }
} 
export const playlistController = new PlaylistController();