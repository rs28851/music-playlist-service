import express from 'express';
import Router from './Router';
import swaggerUi from 'swagger-ui-express';
import * as swaggerDocument from './swagger.json';
import * as bodyParser from 'body-parser';


class App {
  public app: express.Express;

  constructor() {
    this.app = express();

    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json());
    
    new Router(this.app);

    this.app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));     
  }

  public listen(port: number, callbackFn: () => void): void {
    this.app.set('port', port);
    this.app.listen(port, callbackFn);
  }
}

export const application = new App();
