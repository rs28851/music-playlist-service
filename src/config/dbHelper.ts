import mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';

const connectDB = async () => {
    try {
        const mongod = new MongoMemoryServer();

        const uri = await mongod.getUri('musicPlaylist');
        const mongooseOpts = {
            useNewUrlParser: true,
            useUnifiedTopology: true
        };
        await mongoose.connect(uri, mongooseOpts);
        
    } catch (err) {
        console.error(err.message);
        process.exit(1);
    }
};

export default connectDB;