import * as express from 'express';
import { playlistController } from './controller/PlaylistController';

class Router {

    constructor(server: express.Express) {
        const router = express.Router();

        router.get('/', (req: express.Request, res: express.Response) => {
            res.redirect('/swagger');
        })
        
        //create a playlist for a user
        router.post('/user/:userId/playlist', playlistController.createPlaylist.bind(playlistController));

        //get all playlist for a user
        router.get('/user/:userId/playlist', playlistController.getAllPlaylist.bind(playlistController));

        //update a playlist for a user
        router.put('/user/:userId/playlist/:playlistId', playlistController.updatePlaylist.bind(playlistController));

        //delete a playlist for a user
        router.delete('/user/:userId/playlist/:playlistId', playlistController.deletePlaylist.bind(playlistController));

        // //add an article into a playlist for a user
        // router.put('/user/:userId/playlist/:playlistId/article/:articleId/actions/addArticle', playlistController.addArticleIntoPlaylist.bind(playlistController));

        // //remove an article from a playlist for a user
        // router.put('/user/:userId/playlist/:playlistId/article/:articleId/actions/removeArticle', playlistController.removeArticleFromPlaylist.bind(playlistController));
        
        // //get playlist with article information for a user
        // router.get('/user/:userId/playlist/:playlistId', playlistController.getPlayListWithArticle.bind(playlistController));
        server.use('/', router);
    }
}

export default Router;