export class ResourceConflictError {
    public message: string;
    public statusCode: number;

    constructor(msg?: string) {
        this.message = `resource already present: ${msg}`;
        this.statusCode = 409;
    }
}