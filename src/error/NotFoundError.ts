export class NotFoundError {
    public message: string;
    public statusCode: number;

    constructor(msg?: string) {
        this.message = `data not found: ${msg}`;
        this.statusCode = 404;
    }
}