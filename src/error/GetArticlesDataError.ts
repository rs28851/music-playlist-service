import { AxiosError } from "axios";

export class GetArticlesDataError {
    public message: string;
    public statusCode: number;
    public statusText: string; 


    constructor(error: AxiosError) {
        this.message = `error occurred while getting articles data: ${error}`;
        this.statusCode = error.response.status;
        this.statusText = error.response.statusText;
    }
}