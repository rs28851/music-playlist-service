export class InternalServerError {
    public message: string;
    public statusCode: number;

    constructor(msg?: string) {
        this.message = `Internal Server Error: ${msg}`;
        this.statusCode = 500;
    }
}