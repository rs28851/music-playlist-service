export class BadRequestError {
    public message: string;
    public statusCode: number;

    constructor(msg?: string) {
        this.message = `Get invalid data or missing data in request: ${msg ? msg : ''}`;
        this.statusCode = 400;
    }
}