import { IAllPlayListResponse, Playlist, IPlaylist, IAddPlayListData } from "../models/Playlist";
import * as uuid from 'uuid';
import { NotFoundError } from "../error/NotFoundError";
import { ResourceConflictError } from "../error/ResourceConflictError";
import { IPlaylistArticlesResponse, IArticleResponse } from "../models/Articles";
import { InternalServerError } from "../error/InternalServerError";
import { IAddUserData, IUser, User } from "../models/User";
import { GetArticlesDataError } from "../error/GetArticlesDataError";

class PlayListService {

    public async createPlayList(addPlayListData: IAddPlayListData): Promise<void> {
        const playListData = {
            id: uuid.v4(),
            name: addPlayListData.playListName,
            userId: addPlayListData.userId,
        };
        const userData: IAddUserData = {
            userId: addPlayListData.userId,
            userName: Math.random().toString(20).substr(2, 6)
        };
        
        try {
            const playlist: IPlaylist = new Playlist(playListData);
            await playlist.save();
            const user: IUser = await User.findOne({userId: addPlayListData.userId});
            if (!user) {
                const user: IUser = new User(userData);
                await user.save();   
            }
        } catch (e) {
            if (e.message) {
                if (e.message.includes('duplicate key')) {
                    throw new ResourceConflictError(`Playlist already present with name: ${addPlayListData.playListName} for user id: ${addPlayListData.userId}`);
                }
            }
            throw new InternalServerError();
        }
    }

    public async getAllPlayListData(userId: string): Promise<IAllPlayListResponse[]> {
        const allPlaylist: IAllPlayListResponse[] = [];
        
        const allPlayListData: IPlaylist[] = await Playlist.find({userId: userId});
        if (allPlayListData.length <= 0) {
            console.log(`No Playlist found for this userId: ${userId}. For creating a playlist for a user you can use create playlist api which will also ensure that user will be present.`);
        }
        const userData: IUser = await User.findOne({userId: userId});
        if (!userData) {
            console.log(`User not found for this userId: ${userId}. For creating a user  you can use create playlist api which will ensure that user will be present.`);
        }
        for (const playlist of allPlayListData) {
            allPlaylist.push({
                playlistId: playlist.id,
                playlisName: playlist.name,
                userId: playlist.userId,
                userName: userData.userName,
                articleIds: playlist.articleIds
            });
        }

        return allPlaylist;
    }

    public async updatePlaylistData(playListId: string, userId: string, newPlayListName: string): Promise<void> {
        
        const playListData: IPlaylist = await Playlist.findOne({id: playListId, userId: userId});
        if (!playListData) {
            throw new NotFoundError(`playlist not found for playlistId ${playListId} and userId ${userId}`);
        }
        playListData.name = newPlayListName;
        try {
            await new Playlist(playListData).save();
        } catch (e) {
            if (e.message) {
                if (e.message.includes('duplicate key')) {
                    throw new ResourceConflictError(`Playlist already present with name: ${playListData.name} for user id: ${userId}`);
                }
            }
            throw new InternalServerError();
        }
    }

    public async deletePlaylist(playListId: string, userId: string): Promise<void> {

        const playListData: IPlaylist = await Playlist.findOne({id: playListId, userId: userId});
        if (!playListData) {
            throw new NotFoundError(`playlist not found for playlistId ${playListId} and userId ${userId}`);
        }
        await Playlist.deleteOne({id: playListId, userId: userId});
    }

    public async addArticleIntoPlaylist(userId: string, playListId: string, articleId: number): Promise<void> {

        const playListData: IPlaylist = await Playlist.findOne({id: playListId, userId: userId});
        if (!playListData) {
            throw new NotFoundError(`playlist not found for playlistId ${playListId} and userId ${userId}`);
        }
        if (playListData.articleIds.includes(articleId)) {
            throw new ResourceConflictError(`article with article id ${articleId} already exist for playlist id ${playListId} and userId ${userId}`);
        }
        playListData.articleIds.push(articleId);
        await new Playlist(playListData).save();
    }

    public async removeArticleFromPlaylist(userId: string, playListId: string, articleId: number): Promise<void> {
        
        const playListData: IPlaylist = await Playlist.findOne({id: playListId, userId: userId});
        if (!playListData) {
            throw new NotFoundError(`playlist not found for playlistId ${playListId} and userId ${userId}`);
        }
        const articleIndex = playListData.articleIds.indexOf(articleId);
        if (articleIndex > -1) {
            playListData.articleIds.splice(articleIndex, 1);
        }
        await new Playlist(playListData).save();
    }
}

export const playListService = new PlayListService();